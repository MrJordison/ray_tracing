package SimpleRaytracer.io;

import SimpleRaytracer.Math.mat.Matrix;
import SimpleRaytracer.Rendering.Color;
import SimpleRaytracer.Rendering.Texture;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class TextureIO {
    public static Texture in(String filename){
        try {
            //Création des instances d'objets nécéssaire au chargement et lecture de l'Texture source
            BufferedImage imgfile = javax.imageio.ImageIO.read(new File(filename));

            //Récupération des dimensions de l'Texture pour la création du framebuffer
            int[] dimensions = new int[]{imgfile.getHeight(), imgfile.getWidth()};

            Matrix<Color> framebuffer = new Matrix<Color>(dimensions);


            java.awt.Color c_temp;
            //Création de la matrice de pixels qui est passée comme paramètre à l'instance d'Texture
            for (int i = 0; i < dimensions[0]; ++i)
                for (int j = 0; j < dimensions[1]; ++j) {
                    c_temp = new java.awt.Color(imgfile.getRGB(j, i));
                    framebuffer.set_value(i, j, convertToCoreColor(c_temp));
                }

            return new Texture(framebuffer);
        }
        catch(IOException e){
            e.printStackTrace();
        }

        return null;
    }

    public static void out(Texture Texture, String filename){
        try{
            int[] dimensions = Texture.getDimensions();
            BufferedImage imgfile = new BufferedImage(dimensions[1], dimensions[0], BufferedImage.TYPE_INT_RGB);
            java.awt.Color c_temp;
            for(int i = 0 ; i < dimensions[0]; ++i)
                for(int j = 0 ; j < dimensions[1]; ++j){
                    c_temp = convertToJavaColor(Texture.getPixel(i,j));
                    imgfile.setRGB(j,i,c_temp.getRGB());
                }
            javax.imageio.ImageIO.write(imgfile, "png", new File(filename));
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static Color convertToCoreColor(java.awt.Color c){
        return new Color(c.getRed() / 255.f, c.getGreen() / 255.f, c.getBlue() / 255.f);
    }

    public static java.awt.Color convertToJavaColor(Color c){
        Color cl = c.clamp(0.,1.);
        return new java.awt.Color((float)cl.m_Red, (float)cl.m_Green, (float)cl.m_Blue);
    }
}
