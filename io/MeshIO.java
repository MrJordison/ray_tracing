package SimpleRaytracer.io;

import SimpleRaytracer.Geometry.Triangle;
import SimpleRaytracer.Math.Vector;
import SimpleRaytracer.Rendering.Texture;
import SimpleRaytracer.Rendering.Utils;
import com.owens.oobjloader.builder.*;
import com.owens.oobjloader.parser.Parse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MeshIO {

    public static List<Triangle> ObjPtsIn(String filename, Vector obj_pos){
        List<Triangle> result = null;

        List<Vector> normales_buffer;
        List<Vector> pts_buffer;
        List<Vector> uvs_buffer;
        try {

            Build build = new Build();
            Parse parser = new Parse(build, new File(filename).toURL());

            result = new ArrayList<>();

            HashMap<String, Texture> textures = new HashMap<>();

            //Récupération des faces de la mesh
            for(Face triangle : build.faces){

                pts_buffer = new ArrayList<>();
                normales_buffer = new ArrayList<>();
                uvs_buffer = new ArrayList<>();

                for(int i = 0 ; i < 3 ; ++i) {
                    pts_buffer.add(ConvertToCoreVector(triangle.vertices.get(i).v).add(obj_pos));
                    if(triangle.vertices.get(i).n != null)
                        normales_buffer.add(ConvertToCoreVector(triangle.vertices.get(i).n));
                    if(triangle.vertices.get(i).t != null)
                        uvs_buffer.add(ConvertToCoreVector(triangle.vertices.get(i).t));
                }
                if(normales_buffer.size() == 0)
                    normales_buffer = null;
                result.add(new Triangle(pts_buffer, normales_buffer));
                if(uvs_buffer.size() != 0)
                    result.get(result.size()-1).setUVCoords(uvs_buffer);

                //Récupération texture si n'existe pas dans le Map et face en possède une
                /*if(triangle.material.mapKdFilename!= null){
                    if(!textures.containsKey(triangle.material.mapKdFilename)){
                        //Import spécial si fichier d'extension tga
                        if(Utils.testFileExtension(triangle.material.mapKdFilename,"tga")){
                            //textures.put(triangle.material.mapKdFilename,
                            //        TargaReader.getTexture(filename.substring(0,filename.lastIndexOf("/")+1)+triangle.material.mapKdFilename));
                        }
                        //Sinon import via TextureIO
                        else{
                            textures.put(triangle.material.mapKdFilename, TextureIO.in(filename.substring(0,filename.lastIndexOf("/")+1)+triangle.material.mapKdFilename));
                        }
                    }
                    //La texture étant présente dans la table,on passe directement la référence au triangle
                    result.get(result.size()-1).setTexture(textures.get(triangle.material.mapKdFilename));
                }*/
            }


        }
        catch(IOException e){
            e.printStackTrace();
        }

        return result;
    }

    public static Vector ConvertToCoreVector(VertexGeometric v){
        return new Vector(v.x, v.y, v.z);
    }

    public static Vector ConvertToCoreVector(VertexNormal v){
        return new Vector(v.x, v.y, v.z);
    }

    public static Vector ConvertToCoreVector(VertexTexture v){
        return new Vector(v.u, v.v, 0.);
    }


}
