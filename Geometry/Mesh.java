package SimpleRaytracer.Geometry;

import SimpleRaytracer.Geometry.csg.CSGThing;
import SimpleRaytracer.Material.Material;
import SimpleRaytracer.Math.Ray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Mesh implements Thing, CSGThing {

    private List<Triangle> faces;

    private Material material;

    public Mesh(List<Triangle> triangles){
        setFaces(triangles);
        material = null;
    }

    public Mesh(List<Triangle> triangles, Material material){
        setFaces(triangles);
        setMaterial(material);
    }

    public List<Triangle> getFaces(){return faces;}

    public Material getMaterial(){return material;}

    public boolean setFaces(List<Triangle> triangles){
        if(triangles == null){
            System.err.println("NullPointerError : "+(Object)this.toString() + "\nLa liste de facettes est nulle");
            return false;
        }
        if(triangles.size() == 0){
            System.err.println("FacesListSizeError : "+(Object)this.toString() + "\nLa liste de facettes est vide");
            return false;
        }
        for(Triangle face : triangles){
            if(face == null){
                System.err.println("NullPointerError : "+(Object)this.toString() + "\nTentative d'ajouter une facette nulle");
                return false;
            }
        }
        this.faces = triangles;
        return true;
    }

    public boolean setMaterial(Material material){
        if(material == null) {
            System.err.println("NullPointerError : "+(Object)this.toString() + "\nTentative de changer le Material à null");
            return false;
        }
        this.material = material;

        //Application du material sur l'ensemble des triangles (objet mono material)
        for(Triangle t : faces)
            t.setMaterial(material);
        return true;
    }

    /**
     * Itère sur tous les faces de la mesh, récupère les instances d'intersections et choisi celle qui est la plus proche
     * @param ray
     * @return
     */
    @Override
    public Intersection intersect(Ray ray) {
        Intersection result = null;
        Intersection temp;

        //Itération sur toutes les faces du triangles
        for(Triangle face : faces) {
            //Calcul de l'intersection avec la facette et le rayon
            temp = face.intersect(ray);

            //Si il y a intersection
            if(temp != null){

                //Si il n'y pas encore eu d'intersection, on la conserve
                if(result == null)
                    result = temp;

                //Sinon, on regarde si elle est plus proche, auquel cas c'est celle ci que l'on conserve
                else{
                    if(result.m_RayDistance > temp.m_RayDistance)
                        result = temp;
                }
            }
        }

        return result;
    }
    
    /**
     * Itère sur tous les faces de la mesh, retourne les instances d'intersections trié par distance croissantes 
     * @param ray
     * @return
     */
    public List<Intersection> intersectCSG(Ray ray) {
        List<Intersection> result_intersections = new ArrayList<>();
        Intersection temp;

        //Itération sur toutes les faces du triangles
        for(Triangle face : faces) {
            //Calcul de l'intersection avec la facette et le rayon
            temp = face.intersect(ray);

            //Si il y a intersection
            if(temp != null){
                result_intersections.add(temp);
            }
        }
        Collections.sort(result_intersections);
    
        return result_intersections;
    }

    @Override
    public boolean isSolid() {
        return false;
    }
}
