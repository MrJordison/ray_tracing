package SimpleRaytracer.Geometry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import SimpleRaytracer.Material.Material;
import SimpleRaytracer.Math.Ray;
import SimpleRaytracer.Math.Vector;
import SimpleRaytracer.Rendering.Texture;

public class Triangle implements Thing{

    /*Attributs*/

    private List<Vector> vertices;

    private List<Vector> normales;

    private List<Vector> uv_coords;

    private Material material;

    private Texture texture;

    /*Constructeurs*/

    public Triangle(Vector p1, Vector p2, Vector p3){
		List<Vector> vertices = new ArrayList<>();
		vertices.add(p1);
		vertices.add(p2);
		vertices.add(p3);
		construct(vertices, null, null, null);
	}

	public Triangle(Vector p1, Vector p2, Vector p3, Material material){
		List<Vector> pts = new ArrayList<>();
		pts.add(p1);
		pts.add(p2);
		pts.add(p3);
		construct(pts, null, null, material);
	}

	public Triangle(List<Vector> pts){
		construct(pts, null, null, null);
	}

	public Triangle(List<Vector> pts, Material material){
		construct(pts, null, null, material);
	}

	public Triangle(List<Vector> pts, List<Vector> normales){
		construct(pts, normales, null, null);
	}

	public Triangle(List<Vector> vertices, List<Vector> normales, List<Vector> uv_coords){construct(
	        vertices, normales, uv_coords, null);
	}

	public Triangle(List<Vector> pts, List<Vector> normales, Material material){
		construct(pts, normales, null, material);
	}

	private boolean construct(List<Vector> pts, List<Vector> normales, List<Vector> uv_coords, Material material){
		if(pts.size() != 3){
			System.out.println("Erreur nombre de points différent de 3");
			return false;
		}
		for(Vector p : pts)
			if(p == null){
				System.out.println("Point null");
				return false;
			}
		this.vertices = pts;
		this.material = material;
		//Si il n'y a pas de normales passées en paramétre, considére que la normale de chaque point est la normale de la facette (triangle)
		if(normales == null){
			this.normales = new ArrayList<Vector>();
			Vector facette_normale = pts.get(1).sub(pts.get(0)).cross(pts.get(2).sub(pts.get(0)));
			for(int i = 0 ; i < 3 ; ++i)
				this.normales.add(facette_normale);
		}
		else
			this.normales = normales;
		this.uv_coords = uv_coords;
		return true;
	}

	/*Getters, setters*/

	/*Getters, setters*/
	public List<Vector> getVertices(){
	    return vertices;
    }
    
    public List<Vector> getNormales(){
	    return normales;
    }
    
    public List<Vector> getUVCoords(){
        return uv_coords;
    }
    
    @Override
    public Material getMaterial(){
        return material;
    }

    public Texture getTexture(){return texture;}
	
    public boolean setVertices(List<Vector> vertices){
        if(vertices.size() != 3) {
            System.err.println("SizeVerticesListError : "+(Object)this.toString() + "\nLe nouveau nombre de vertices est différent de 3");
            return false;
        }
        for(int i = 0 ; i < 3 ; ++i)
            if(vertices.get(i) == null) {
                System.err.println("NullPointerError : "+(Object)this.toString() + "\nTentative d'ajouter une vertice nulle");
                return false;
            }
        this.vertices = vertices;
        return true;
    }

    public boolean setNormales(List<Vector> normales){
        if(normales.size() != 3) {
            System.err.println("SizeNormalesListError : "+(Object)this.toString() + "\nLe nouveau nombre de normales est différent de 3");
            return false;
        }
        for(int i = 0 ; i < 3 ; ++i)
            if(normales.get(i) == null) {
                System.err.println("NullPointerError : "+(Object)this.toString() + "\nTentative d'ajouter une normale nulle");
                return false;
            }
        this.normales = normales;
        return true;
    }

    public boolean setUVCoords(List<Vector> uv_coords){
        if(uv_coords.size() != 3) {
            System.err.println("SizeUVCoordsListError : "+(Object)this.toString() + "\nLe nouveau nombre de coordonnées de textures est différent de 3");
            return false;
        }
        for(int i = 0 ; i < 3 ; ++i)
            if(uv_coords.get(i) == null) {
                System.err.println("NullPointerError : "+(Object)this.toString() + "\nTentative d'ajouter une coordonnée de texture nulle");
                return false;
            }
        this.uv_coords = uv_coords;
        return true;
    }

    public boolean setMaterial(Material material){
        if(material == null) {
            System.err.println("NullPointerError : "+(Object)this.toString() + "\nTentative de changer le Material à null");
            return false;
        }
        this.material = material;
        return true;
    }

    public boolean setTexture(Texture texture){
        if(texture == null){
            this.texture = texture;
            return false;
        }
        this.texture = texture;
        return true;
    }

    /**
     * Fonction récupérant la normale associée au vertice passé en paramètre
     * Gère le cas ou le point n'appartient pas à la liste des vertices (affiche un message d'erreur et renvoie null)
     * @param p
     * @return
     */
    public Vector getNormaleVertice(Vector p){
        for(int i = 0 ; i < vertices.size(); ++i)
            if(vertices.get(i) == p)
                return normales.get(i);
        System.err.println("BelongVerticeError : "+(Object)this.toString() + "\nLe vertice passé en paramètre n'appartient pas au triangle");
        return null;
    }

    /**
     * Fonction récupérant la coordonnée de texture associée au vertice passé en paramètre
     * Gère le cas ou le point n'appartient pas à la liste des vertices (affiche un message d'erreur et renvoie null)
     * @param p
     * @return
     */
    public Vector getUVCoordVertice(Vector p){
        for(int i = 0 ; i < vertices.size(); ++i)
            if(vertices.get(i) == p)
                return uv_coords.get(i);
        System.err.println("BelongVerticeError : "+(Object)this.toString() + "\nLe vertice passé en paramètre n'appartient pas au triangle");
        return null;
    }

    /**
     * Récupère les coordonnées minimum de la boite englobante du triangle
     * @return
     */
    public Vector getMin(){
        Vector result = new Vector(Double.POSITIVE_INFINITY);

        for(Vector v : vertices){
            if(v.m_X < result.m_X)
                result.m_X = v.m_X;
            if(v.m_Y < result.m_Y)
                result.m_Y = v.m_Y;
            if(v.m_Z < result.m_Z)
                result.m_Z = v.m_Z;
        }
        return result;
    }

    /**
     * Récupère les coordonées maximum de la boite englobante du triangle
     * @return
     */
    public Vector getMax(){
        Vector result = new Vector(Double.NEGATIVE_INFINITY);

        for(Vector v : vertices){
            if(v.m_X > result.m_X)
                result.m_X = v.m_X;
            if(v.m_Y > result.m_Y)
                result.m_Y = v.m_Y;
            if(v.m_Z > result.m_Z)
                result.m_Z = v.m_Z;
        }
        return result;
    }

    /**
     * Fonction calculant les coefficients barycentriques d'un point dans le triangle
     * Par convention, le point appartient déjà au plan
     * Le cumul des trois coefficients est égal à 1 . (dans le cas où le point est dans le triangle)
     * @param p
     * @return Les valeurs des coefficients barycentriques sous la forme d'un Vecteur
     */
    public Vector getBarycentricsCoeffs(Vector p){

        //Vecteurs des côtés AB / AC du triangle ABC
        Vector side1 = vertices.get(1).sub(vertices.get(0));
        Vector side2 = vertices.get(2).sub(vertices.get(0));

        //Normale du triangle / plan selon les côtés AB / AC
        Vector Ntriangle = side1.cross(side2);
        double triangle_area = Ntriangle.dot(Ntriangle);


        Vector edge0, edge1, edge2, edge0P, edge1P, edge2P, Ntest;
        edge0 = side1;
        edge1 = vertices.get(2).sub(vertices.get(1));
        edge2 = vertices.get(0).sub(vertices.get(2));
        edge0P = p.sub(vertices.get(0));
        edge1P = p.sub(vertices.get(1));
        edge2P = p.sub(vertices.get(2));



        Ntest = edge0.cross(edge0P);
        double u = Ntriangle.dot(Ntest);
        if(u < 0) return null; // P est à l'extérieur du cété 0 du triangle

        Ntest = edge1.cross(edge1P);
        double v = Ntriangle.dot(Ntest);
        if(v < 0) return null; // P est à l'extérieur du côté 1 du triangle

        Ntest = edge2.cross(edge2P);
        double w = Ntriangle.dot(Ntest);
        if(w < 0) return null; // P est é l'extérieur du côté 2 du triangle

        //A cette étape il a été prouvé que le point d'intersection p intersecte bien le plan du triangle
        //et que celui ci est situé é l'intérieur. On peut donc retourner une instance d'Intersection

        //Il faut cependant calculer la normale au point d'intersection, dans le cas d'un triangle interpoller les normales aux sommets
        //avec les coefficients barycentriques trouvés précédemment.

        return new Vector(v / triangle_area,w / triangle_area,u / triangle_area);
    }

	@Override
	public Intersection intersect(Ray ray) {
		Vector ray_pos = ray.m_Origin;

		//Vecteurs des côtés AB / AC du triangle ABC
		Vector side1 = vertices.get(1).sub(vertices.get(0));
		Vector side2 = vertices.get(2).sub(vertices.get(0));

		//Normale du triangle / plan selon les côtés AB / AC
		Vector Ntriangle = side1.cross(side2);
		double triangle_area = Ntriangle.dot(Ntriangle);

		//futur point d'intersection entre le rayon et le plan du triangle
		Vector p;
		boolean test;

		//1ere étape : calcul de l'intersection P entre le plan du triangle et le rayon

		//Regarde si le rayon est parallèle au plan : si oui alors il n'y a pas d'intersection possible
		double dirRayPlan = Ntriangle.dot(ray.m_Direction);
		test = (dirRayPlan == 0);
		if(test) return null;

		Vector vectorPTSRayonOrigin = ray.m_Origin.sub(vertices.get(0));
		double angleNormalVecteurPtsRayons = - Ntriangle.dot(vectorPTSRayonOrigin);

		double d = Ntriangle.dot(vertices.get(0));

		//Calcul de la distance t => distance entre l'origine du rayon et le plan
		double t = angleNormalVecteurPtsRayons / dirRayPlan;
		if(t == Double.POSITIVE_INFINITY)
		    t = 0.;

		//Si t est négatif alors cela signifie que le plan est situé derrière l'origine
		test = t < 0 ;
		if(test) return null; // regarde si le triangle est derrière l'origine du rayon => pas d'intersection possible

		//Calcul du vecteur P
		p = ray.m_Origin.add(ray.m_Direction.mul(t));

		//2e étape : test si le point d'intersection est situé à l'intérieur du triangle
		Vector sideP = p.sub(vertices.get(0));

		//Regarde avec chaque côté si cross product dans le même sens que normale du plan Ntriangle
		//si ce n'est pas le cas, le point d'intersection est à l'extérieur du triangle et donc ne l'intersecte pas
		Vector edge0, edge1, edge2, edge0P, edge1P, edge2P, Ntest;
		edge0 = side1;
		edge1 = vertices.get(2).sub(vertices.get(1));
		edge2 = vertices.get(0).sub(vertices.get(2));
		edge0P = p.sub(vertices.get(0));
		edge1P = p.sub(vertices.get(1));
		edge2P = p.sub(vertices.get(2));

		Ntest = edge0.cross(edge0P);
		double u = Ntriangle.dot(Ntest);
        if(u < 0) return null; // P est é l'extérieur du cété 0 du triangle

		Ntest = edge1.cross(edge1P);
        double v = Ntriangle.dot(Ntest);
		if(v < 0) return null; // P est é l'extérieur du cété 1 du triangle

		Ntest = edge2.cross(edge2P);
        double w = Ntriangle.dot(Ntest);
		if(w < 0) return null; // P est é l'extérieur du cété 2 du triangle

		//A cette étape il a été prouvé que le point d'intersection p intersecte bien le plan du triangle
		//et que celui ci est situé é l'intérieur. On peut donc retourner une instance d'Intersection

		//Il faut cependant calculer la normale au point d'intersection, dans le cas d'un triangle interpoller les normales aux sommets
		//avec les coefficients barycentriques trouvés précédemment.
		Vector inters_normale = getNormaleVertice(getVertices().get(2)).mul(u/triangle_area)
				.add(getNormaleVertice(getVertices().get(0)).mul(v/triangle_area))
				.add(getNormaleVertice(getVertices().get(1)).mul(w/triangle_area));

		return new Intersection(this, ray, t, inters_normale);
    }

    @Override
    public boolean isSolid() {
            // TODO Auto-generated method stub
            return false;
    }

    public String toString(){
        return "["+vertices.get(0)+"; "+vertices.get(1)+"; "+vertices.get(2)+"]";
    }

    /**
     * Fonction de tri de Triangle selon une coordonnée choisie (x, y ou z) ainsi que l'ordre (croissant / décroissant)
     * @param triangles
     * @param axis
     * @param order
     */
    public static void sortBy(List<Triangle> triangles, String axis, int order){
        if(axis == "x"){
            if(order == 0) // ASCENDING
            Collections.sort(triangles, (Triangle t, Triangle t2)->{

                Vector v, v2;
                v = t.getMin();
                v2 = t2.getMin();
                if(v.m_X < v2.m_X)
                    return -1;
                else if(v.m_X == v2.m_X)
                    return 0;
                return 1;
            });

            else if(order == 1) // DESCENDING
                Collections.sort(triangles, (Triangle t, Triangle t2)->{
                    Vector v, v2;
                    v = t.getMax();
                    v2 = t2.getMax();
                    if(v.m_X > v2.m_X)
                        return -1;
                    else if(v.m_X == v2.m_X)
                        return 0;
                    return 1;
                });
        }
        else if(axis == "y"){
            if(order == 0) // ASCENDING
                Collections.sort(triangles, (Triangle t, Triangle t2)->{

                    Vector v, v2;
                    v = t.getMin();
                    v2 = t2.getMin();
                    if(v.m_Y < v2.m_Y)
                        return -1;
                    else if(v.m_Y == v2.m_Y)
                        return 0;
                    return 1;
                });

            else if(order == 1) // DESCENDING
                Collections.sort(triangles, (Triangle t, Triangle t2)->{
                    Vector v, v2;
                    v = t.getMax();
                    v2 = t2.getMax();
                    if(v.m_Y > v2.m_Y)
                        return -1;
                    else if(v.m_Y == v2.m_Y)
                        return 0;
                    return 1;
                });
        }
        else if(axis == "z"){
            if(order == 0) // ASCENDING
                Collections.sort(triangles, (Triangle t, Triangle t2)->{

                    Vector v, v2;
                    v = t.getMin();
                    v2 = t2.getMin();
                    if(v.m_Z < v2.m_Z)
                        return -1;
                    else if(v.m_Z == v2.m_Z)
                        return 0;
                    return 1;
                });

            else if(order == 1) // DESCENDING
                Collections.sort(triangles, (Triangle t, Triangle t2)->{
                    Vector v, v2;
                    v = t.getMax();
                    v2 = t2.getMax();
                    if(v.m_Z > v2.m_Z)
                        return -1;
                    else if(v.m_Z == v2.m_Z)
                        return 0;
                    return 1;
                });
        }
    }
}
