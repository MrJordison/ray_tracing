package SimpleRaytracer.Geometry;

import SimpleRaytracer.Material.Material;
import SimpleRaytracer.Math.Ray;
import SimpleRaytracer.Math.Vector;

public interface Thing
{
	public Intersection intersect(Ray ray);
	public Material getMaterial();
	public boolean isSolid();
}
