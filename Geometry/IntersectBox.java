package SimpleRaytracer.Geometry;

import SimpleRaytracer.Math.Ray;
import SimpleRaytracer.Math.Vector;

import java.util.List;

public class IntersectBox {
    Vector minDiag;
    Vector maxDiag;
    Vector center;
    Vector halfDims;

    /**
     * Validé ! (en même temps c'est pas difficile...)
     * @param min
     * @param max
     */
    public IntersectBox(Vector min, Vector max) {
        minDiag = min;
        maxDiag = max;
        center = null;
    }

    /**
     * Validé !
     * @param triangles Ensemble de triangles / facettes à partir duquel on va déterminer les coordonnées de diagMin et diagMax
     */
    public IntersectBox(List<Triangle> triangles) {

        Vector pt;

        double[] values = new double[]{0.,0.,0.,0.,0.,0.};
        for(Triangle t : triangles)
            for(int i = 0 ; i < t.getVertices().size(); ++i){

                pt = t.getVertices().get(i);

                if(pt.m_X <= values[0])
                    values[0] = pt.m_X;

                if(pt.m_X >= values[1])
                    values[1] = pt.m_X;

                if(pt.m_Y <= values[2])
                    values[2] = pt.m_Y;

                if(pt.m_Y >= values[3])
                    values[3] = pt.m_Y;

                if(pt.m_Z <= values[4])
                    values[4] = pt.m_Z;

                if(pt.m_Z >= values[5])
                    values[5] = pt.m_Z;
            }

        minDiag = new Vector(values[0], values[2], values[4]);
        maxDiag = new Vector(values[1], values[3], values[5]);
    }

    /**
     * Validé !
     * @return
     */
    public Vector getCenter() {
        if (center == null)
            center = minDiag.add(maxDiag.sub(minDiag).mul(1. / 2.));
        return center;

    }

    /**
     * Validé !
     * @return
     */
    public Vector getMin(){return minDiag;}

    /**
     * Validé !
     * @return
     */
    public Vector getMax(){return maxDiag;}

    /**
     * Validé !
     * @return
     */
    public Vector getHalfDims() {
        if (halfDims == null) {
            if (center == null)
                getCenter();
            halfDims = new Vector(
                    center.m_X - minDiag.m_X,
                    maxDiag.m_Y - center.m_Y,
                    center.m_Z - minDiag.m_Z
            );
        }
        return halfDims;
    }

    /**
     * Validé !
     * @param position
     * @return
     */
    public IntersectBox getChild(int position) {
        if (halfDims == null)
            getHalfDims();

        Vector min, max;
        switch (position) {
            //cas en haut à gauche moitié supérieure
            case 1:
                min = center.clone();
                min.m_X = min.m_X - halfDims.m_X;
                max = maxDiag.clone();
                max.m_X = max.m_X - halfDims.m_X;
                break;

            //cas en haut à droite moitié supérieure
            case 2:
                min = center.clone();
                max = maxDiag.clone();
                break;

            //cas en bas à gauche moitié supérieure
            case 3:
                min = minDiag.clone();
                min.m_Y = min.m_Y + halfDims.m_Y;
                max = center.clone();
                max.m_Y = max.m_Y + halfDims.m_Y;
                break;
            //cas en bas à droite moitié supérieure
            case 4:
                min = center.clone();
                min.m_Z = min.m_Z - halfDims.m_Z;
                max = maxDiag.clone();
                max.m_Z = max.m_Z - halfDims.m_Z;
                break;
            //cas en haut à gauche moitié inférieure
            case 5:
                min = minDiag.clone();
                min.m_Z = min.m_Z + halfDims.m_Z;
                max = center.clone();
                max.m_Z = max.m_Z + halfDims.m_Z;
                break;
            //cas en haut à droite moitié inférieure
            case 6:
                min = center.clone();
                min.m_Y = min.m_Y - halfDims.m_Y;
                max = maxDiag.clone();
                max.m_Y = max.m_Y - halfDims.m_Y;
                break;
            //cas en bas à gauche moitié inférieure
            case 7:
                min = minDiag.clone();
                max = center.clone();
                break;
            //cas en bas à droite moitié inférieure
            case 8:
                min = minDiag.clone();
                min.m_X = min.m_X + halfDims.m_X;
                max = center.clone();
                max.m_X = max.m_X + halfDims.m_X;
                break;
            default:
                return null;
        }
        return new IntersectBox(min, max);
    }

    public Vector getBoxHalfSize() {
        return maxDiag.sub(getCenter());
    }

    public double getSurfaceArea() {
        Vector s = maxDiag.sub(minDiag);
        return (s.m_X * s.m_Y + s.m_X * s.m_Z + s.m_Y * s.m_Z) * 2.0f;
    }
	
	/*======================== X-tests ========================*/

    private boolean AXISTEST_X01(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb) {
        double p0, p2, min, max, rad;

        p0 = a * v0.m_Y - b * v0.m_Z;
        p2 = a * v2.m_Y - b * v2.m_Z;
        if (p0 < p2) {
            min = p0;
            max = p2;
        } else {
            min = p2;
            max = p0;
        }
        rad = fa * getBoxHalfSize().m_Y + fb * getBoxHalfSize().m_Z;
        if (min > rad || max < -rad)
            return false;
        return true;
    }

    private boolean AXISTEST_X2(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb) {
        double p0, p1, min, max, rad;

        p0 = a * v0.m_Y - b * v0.m_Z;
        p1 = a * v1.m_Y - b * v1.m_Z;

        if (p0 < p1) {
            min = p0;
            max = p1;
        } else {
            min = p1;
            max = p0;
        }
        rad = fa * getBoxHalfSize().m_Y + fb * getBoxHalfSize().m_Z;
        if (min > rad || max < -rad)
            return false;
        return true;
    }

	/*======================== Y-tests ========================*/

    private boolean AXISTEST_Y02(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb) {
        double p0, p2, min, max, rad;

        p0 = -a * v0.m_X + b * v0.m_Z;
        p2 = -a * v2.m_X + b * v2.m_Z;

        if (p0 < p2) {
            min = p0;
            max = p2;
        } else {
            min = p2;
            max = p0;
        }
        rad = fa * getBoxHalfSize().m_X + fb * getBoxHalfSize().m_Z;
        if (min > rad || max < -rad)
            return false;
        return true;
    }

    private boolean AXISTEST_Y1(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb) {
        double p0, p1, min, max, rad;

        p0 = -a * v0.m_X + b * v0.m_Z;
        p1 = -a * v1.m_X + b * v1.m_Z;

        if (p0 < p1) {
            min = p0;
            max = p1;
        } else {
            min = p1;
            max = p0;
        }
        rad = fa * getBoxHalfSize().m_X + fb * getBoxHalfSize().m_Z;
        if (min > rad || max < -rad)
            return false;
        return true;
    }

	/*======================== Z-tests ========================*/

    private boolean AXISTEST_Z12(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb) {
        double p2, p1, min, max, rad;

        p1 = a * v1.m_X - b * v1.m_Y;
        p2 = a * v2.m_X - b * v2.m_Y;

        if (p2 < p1) {
            min = p2;
            max = p1;
        } else {
            min = p1;
            max = p2;
        }
        rad = fa * getBoxHalfSize().m_X + fb * getBoxHalfSize().m_Y;
        if (min > rad || max < -rad)
            return false;
        return true;
    }

    private boolean AXISTEST_Z0(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb) {
        double p0, p1, min, max, rad;

        p0 = a * v0.m_X - b * v0.m_Y;
        p1 = a * v1.m_X - b * v1.m_Y;

        if (p0 < p1) {
            min = p0;
            max = p1;
        } else {
            min = p1;
            max = p0;
        }
        rad = fa * getBoxHalfSize().m_X + fb * getBoxHalfSize().m_Y;
        if (min > rad || max < -rad)
            return false;
        return true;
    }

    private void FINDMINMAX(double x0, double x1, double x2, double[] minmax) {
        minmax[0] = minmax[1] = x0;
        if (x1 < minmax[0]) minmax[0] = x1;
        if (x1 > minmax[1]) minmax[1] = x1;
        if (x2 < minmax[0]) minmax[0] = x2;
        if (x2 > minmax[1]) minmax[1] = x2;
    }

    //
    // Original code:
    // http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/tribox3.txt
    //

    /**
     * Validé !
     * @param t
     * @return
     */
    public boolean intersectTriangle(Triangle t) {
        Vector boxcenter = getCenter();
        Vector boxhalfsize = getBoxHalfSize();
        List<Vector> triverts = t.getVertices();

        //int triBoxOverlap(double boxcenter[3],double boxhalfsize[3],double triverts[3][3])

		/*    use separating axis theorem to test overlap between triangle and box */
		/*    need to test for overlap in these directions: */
		/*    1) the {x,y,z}-directions (actually, since we use the AABB of the triangle */
		/*       we do not even need to test these) */
		/*    2) normal of the triangle */
		/*    3) crossproduct(edge from tri, {x,y,z}-directin) */
		/*       this gives 3x3=9 more tests */
        Vector v0, v1, v2;

        //   double axis[3];

        double[] minmax = new double[2];
        double p0, p1, p2, rad, fex, fey, fez;        // -NJMP- "d" local variable removed
        Vector normal, e0, e1, e2;
		
		/* This is the fastest branch on Sun */
		/* move everything so that the boxcenter is in (0,0,0) */

        v0 = triverts.get(0).sub(boxcenter);
        v1 = triverts.get(1).sub(boxcenter);
        v2 = triverts.get(2).sub(boxcenter);
		
		/* compute triangle edges */

        e0 = v1.sub(v0);      /* tri edge 0 */
        e1 = v2.sub(v1);      /* tri edge 1 */
        e2 = v0.sub(v2);      /* tri edge 2 */
		
		/* Bullet 3:  */
		
		/*  test the 9 tests first (this was faster) */

        fex = Math.abs(e0.m_X);
        fey = Math.abs(e0.m_Y);
        fez = Math.abs(e0.m_Z);

        if (!AXISTEST_X01(v0, v1, v2, e0.m_Z, e0.m_Y, fez, fey)) return false;
        if (!AXISTEST_Y02(v0, v1, v2, e0.m_Z, e0.m_X, fez, fex)) return false;
        if (!AXISTEST_Z12(v0, v1, v2, e0.m_Y, e0.m_X, fey, fex)) return false;

        fex = Math.abs(e1.m_X);
        fey = Math.abs(e1.m_Y);
        fez = Math.abs(e1.m_Z);

        if (!AXISTEST_X01(v0, v1, v2, e1.m_Z, e1.m_Y, fez, fey)) return false;
        if (!AXISTEST_Y02(v0, v1, v2, e1.m_Z, e1.m_X, fez, fex)) return false;
        if (!AXISTEST_Z0(v0, v1, v2, e1.m_Y, e1.m_X, fey, fex)) return false;

        fex = Math.abs(e2.m_X);
        fey = Math.abs(e2.m_Y);
        fez = Math.abs(e2.m_Z);

        if (!AXISTEST_X2(v0, v1, v2, e2.m_Z, e2.m_Y, fez, fey)) return false;
        if (!AXISTEST_Y1(v0, v1, v2, e2.m_Z, e2.m_X, fez, fex)) return false;
        if (!AXISTEST_Z12(v0, v1, v2, e2.m_Y, e2.m_X, fey, fex)) return false;
		
		/* Bullet 1: */
		/*  first test overlap in the {x,y,z}-directions */
		/*  find min, max of the triangle each direction, and test for overlap in */
		/*  that direction -- this is equivalent to testing a minimal AABB around */
		/*  the triangle against the AABB */
		
		/* test in X-direction */
        FINDMINMAX(v0.m_X, v1.m_X, v2.m_X, minmax);
        if (minmax[0] > boxhalfsize.m_X || minmax[1] < -boxhalfsize.m_X) return false;
		
		/* test in Y-direction */
        FINDMINMAX(v0.m_Y, v1.m_Y, v2.m_Y, minmax);
        if (minmax[0] > boxhalfsize.m_Y || minmax[1] < -boxhalfsize.m_Y) return false;
		
		/* test in Z-direction */
        FINDMINMAX(v0.m_Z, v1.m_Z, v2.m_Z, minmax);
        if (minmax[0] > boxhalfsize.m_Z || minmax[1] < -boxhalfsize.m_Z) return false;
		
		/* Bullet 2: */
		/*  test if the box intersects the plane of the triangle */
		/*  compute plane equation of triangle: normal*x+d=0 */

        normal = e0.cross(e1);

        // -NJMP- (line removed here)
        if (!intersectPlane(normal, v0, boxhalfsize))
            return false;    // -NJMP-

        return true;   /* box and triangle overlaps */
    }

    public boolean intersectPlane(Vector normal, Vector vert, Vector maxbox)    // -NJMP-
    {
        int q;


        double v;

        double[] coord_vert = {vert.m_X, vert.m_Y, vert.m_Z};
        double[] coord_norm = {normal.m_X, normal.m_Y, normal.m_Z};
        double[] coord_vmin = new double[3];
        double[] coord_vmax = new double[3];
        double[] coord_maxbox = {maxbox.m_X, maxbox.m_Y, maxbox.m_Z};

        for (q = 0; q <= 2; q++) {
            v = coord_vert[q];                    // -NJMP-
            if (coord_norm[q] > 0.) {
                coord_vmin[q] = -coord_maxbox[q] - v;    // -NJMP-
                coord_vmax[q] = coord_maxbox[q] - v;    // -NJMP-
            } else {
                coord_vmin[q] = coord_maxbox[q] - v;    // -NJMP-
                coord_vmax[q] = -coord_maxbox[q] - v;    // -NJMP-
            }
        }
        Vector vmin = new Vector(coord_vmin[0], coord_vmin[1], coord_vmin[2]);
        Vector vmax = new Vector(coord_vmax[0], coord_vmax[1], coord_vmax[2]);

        if (normal.dot(vmin) > 0.) return false;    // -NJMP-
        if (normal.dot(vmax) >= 0.) return true;    // -NJMP-

        return false;
    }

    //
    // See:
    // http://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
    //
    public boolean intersectRay(Ray r, double[] tminmax) // t : array of 2 doubles containing min and max of Ray parameter value if intersection is found. Can be null.
    {
        double tmin = (minDiag.m_X - r.m_Origin.m_X) / r.m_Direction.m_X;
        double tmax = (maxDiag.m_X - r.m_Origin.m_X) / r.m_Direction.m_X;

        double temp;

        if (tmin > tmax) {
            // Swap tmin and tmax
            temp = tmin;
            tmin = tmax;
            tmax = temp;
        }

        double tymin = (minDiag.m_Y - r.m_Origin.m_Y) / r.m_Direction.m_Y;
        double tymax = (maxDiag.m_Y - r.m_Origin.m_Y) / r.m_Direction.m_Y;

        if (tymin > tymax) {
            // Swap tymin and tymax
            temp = tymin;
            tymin = tymax;
            tymax = temp;
        }

        if ((tmin > tymax) || (tymin > tmax))
            return false;

        if (tymin > tmin)
            tmin = tymin;

        if (tymax < tmax)
            tmax = tymax;

        double tzmin = (minDiag.m_Z - r.m_Origin.m_Z) / r.m_Direction.m_Z;
        double tzmax = (maxDiag.m_Z - r.m_Origin.m_Z) / r.m_Direction.m_Z;

        if (tzmin > tzmax) {
            // Swap tzmin and tzmax
            temp = tzmin;
            tzmin = tzmax;
            tzmax = temp;
        }

        if ((tmin > tzmax) || (tzmin > tmax))
            return false;

        if (tzmin > tmin)
            tmin = tzmin;

        if (tzmax < tmax)
            tmax = tzmax;

        if (tminmax != null) {
            tminmax[0] = tmin;
            tminmax[1] = tmax;
        }

        return true;
    }

    public boolean intersectRay_(Ray ray, double[] tminmax) //, double minDist, double maxDist)
    {
        Vector min = minDiag;
        Vector max = maxDiag;
        Vector invDir = new Vector(1f / ray.m_Direction.m_X, 1f / ray.m_Direction.m_Y, 1f / ray.m_Direction.m_Z);

        boolean signDirX = invDir.m_X < 0;
        boolean signDirY = invDir.m_Y < 0;
        boolean signDirZ = invDir.m_Z < 0;

        Vector bbox = signDirX ? max : min;
        double tmin = (bbox.m_X - ray.m_Origin.m_X) * invDir.m_X;
        bbox = signDirX ? min : max;
        double tmax = (bbox.m_X - ray.m_Origin.m_X) * invDir.m_X;
        bbox = signDirY ? max : min;
        double tymin = (bbox.m_Y - ray.m_Origin.m_Y) * invDir.m_Y;
        bbox = signDirY ? min : max;
        double tymax = (bbox.m_Y - ray.m_Origin.m_Y) * invDir.m_Y;

        if ((tmin > tymax) || (tymin > tmax)) {
            return false;
        }
        if (tymin > tmin) {
            tmin = tymin;
        }
        if (tymax < tmax) {
            tmax = tymax;
        }

        bbox = signDirZ ? max : min;
        double tzmin = (bbox.m_Z - ray.m_Origin.m_Z) * invDir.m_Z;
        bbox = signDirZ ? min : max;
        double tzmax = (bbox.m_Z - ray.m_Origin.m_Z) * invDir.m_Z;

        if ((tmin > tzmax) || (tzmin > tmax)) {
            return false;
        }
        if (tzmin > tmin) {
            tmin = tzmin;
        }
        if (tzmax < tmax) {
            tmax = tzmax;
        }
        //if ((tmin < maxDist) && (tmax > minDist)) {
        return true; //ray.start.add(ray.dir.mul((double)tmin)); //.getPointAtDistance(tmin);
        //}
        //return null;
    }

    public String toString(){
        return minDiag+" "+maxDiag;
    }

}