package SimpleRaytracer.Geometry;

import SimpleRaytracer.Math.Ray;
import SimpleRaytracer.Math.Vector;

public class Intersection implements Comparable
{
	public Thing m_Thing;
	public Ray m_Ray;
	public double m_RayDistance;
	public Vector m_Normal;
	
	public Intersection(Thing thing, Ray ray, double dist, Vector normal)
	{
		this.m_Thing = thing;
		this.m_Ray = ray;
		this.m_RayDistance = dist;
		this.m_Normal = normal;
	}

	@Override
	public int compareTo(Object o) {
		Intersection i = (Intersection) o;
		if(m_RayDistance < i.m_RayDistance)
			return -1;
		if(m_RayDistance > i.m_RayDistance)
			return 1;
		return 0;
	}
}
