package SimpleRaytracer.Geometry.accel_structs;

import SimpleRaytracer.Geometry.IntersectBox;
import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Geometry.Thing;
import SimpleRaytracer.Geometry.Triangle;
import SimpleRaytracer.Material.Material;
import SimpleRaytracer.Math.Ray;

import java.util.ArrayList;
import java.util.List;

public class BVH implements Thing {

    static final int BVH_MAX_DEPTH = 8;
    static final int BVH_MIN_TRIANGLES = 15;

    BVH parent;
    List<BVH> enfants;
    List<Triangle> triangles;
    Material material;
    IntersectBox boite;

    //constructeur pour le parent
    public BVH(List<Triangle> triangles){
        this.parent = null;
        this.triangles = triangles;
        enfants = null;
        boite = new IntersectBox(triangles);
        material = null;
    }

    public BVH(List<Triangle> triangles, IntersectBox box, BVH parent){
        this.parent = parent;
        this.triangles = triangles;
        enfants = null;
        boite = box;
        material = null;
    }


    @Override
    public Intersection intersect(Ray ray) {
        Intersection result, temp_inters;
        result = temp_inters = null;
        double[] tab = new double[6];

        if(boite.intersectRay(ray, tab)){

            //Si c'est un noeud
            if(!isLeaf()){
                for(BVH child : enfants){
                    temp_inters = child.intersect(ray);
                    if(temp_inters != null) {
                        if (result == null)
                            result = temp_inters;
                        else if(result != null && temp_inters.m_RayDistance < result.m_RayDistance)
                            result = temp_inters;
                    }
                }
            }

            //Si c'est une feuille
            else{
                //System.out.println("Feuille !");
                for(Triangle t : triangles){
                    temp_inters = t.intersect(ray);
                    if(temp_inters != null) {
                        if (result == null)
                            result = temp_inters;
                        else if(result != null && temp_inters.m_RayDistance < result.m_RayDistance)
                            result = temp_inters;
                    }
                }
            }


        }//Fin de condition sur l'intersection avec la boite englobante

        return result;
    }

    @Override
    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material){this.material = material;}

    public IntersectBox getBox(){
        return boite;
    }

    public void runGeneration(){
        System.out.println("Noeud à profondeur "+getDepth());
        if(!isLeaf()) {

            double min_cost, cost, total_area;
            total_area = boite.getSurfaceArea();
            IntersectBox left_box, right_box, min_left_box, min_right_box;

            //Initialisation des variables où est stocké le résultat de recherche
            min_cost = Double.POSITIVE_INFINITY;
            int index = 0;
            String axis = "x";
            min_left_box = new IntersectBox(triangles.subList(0,1));
            min_right_box = new IntersectBox(triangles.subList(1,triangles.size()));

            System.out.println("Tri sur x");

            //Tri sur l'axe des x
            Triangle.sortBy(triangles, "x", 0);
            for (int i = 1; i < triangles.size()-1; ++i) {
                if((i%(triangles.size()/100)) == 0)
                    System.out.println(i + " triangles parcourus sur "+triangles.size());
                left_box = new IntersectBox(triangles.subList(0, i + 1));
                right_box = new IntersectBox(triangles.subList(i + 1, triangles.size()));
                cost = (left_box.getSurfaceArea() + right_box.getSurfaceArea()) / total_area;

                if (cost < min_cost) {
                    cost = min_cost;
                    index = i;
                    axis = "x";
                    min_left_box = left_box;
                    min_right_box = right_box;
                }
            }

            System.out.println("Tri sur y");
            //Tri sur l'axe des y
            Triangle.sortBy(triangles, "y", 0);
            for (int i = 0; i < triangles.size()-1; ++i) {
                left_box = new IntersectBox(triangles.subList(0, i + 1));
                right_box = new IntersectBox(triangles.subList(i + 1, triangles.size()));
                cost = (left_box.getSurfaceArea() + right_box.getSurfaceArea()) / total_area;

                if (cost < min_cost) {
                    cost = min_cost;
                    index = i;
                    axis = "y";
                }
            }

            System.out.println("Tri sur z");
            //Tri sur l'axe des z
            Triangle.sortBy(triangles, "z", 0);
            for (int i = 0; i < triangles.size()-1; ++i) {
                left_box = new IntersectBox(triangles.subList(0, i + 1));
                right_box = new IntersectBox(triangles.subList(i + 1, triangles.size()));

                cost = (left_box.getSurfaceArea() + right_box.getSurfaceArea()) / total_area;

                if (cost < min_cost) {
                    cost = min_cost;
                    index = i;
                    axis = "y";
                }
            }

            //Fin de la recherche de la meilleur séparation de l'ensemble des triangles
            //création des enfants d'après le résultat obtenu

            Triangle.sortBy(triangles, axis, 0);
            enfants = new ArrayList<>();
            enfants.add(new BVH(triangles.subList(0, index + 1), min_left_box, this));
            enfants.add(new BVH(triangles.subList(index + 1, triangles.size()), min_right_box, this));

            //Lancement de la génération des enfants
            for(BVH child : enfants)
                child.runGeneration();
        }
        else
            System.out.println("Atteinte de la génération maximale avec une profondeur de "+getDepth()+" et englobe "+triangles.size()+" triangles");
    }

    //TODO
    public int getDepth(){
        int res = 1;
        if(parent != null)
            res += parent.getDepth();
        return res;
    }

    //TODO
    public boolean isLeaf(){
        if(getDepth() >= BVH_MAX_DEPTH)
            return true;
        if(triangles != null)
            if(triangles.size() <= BVH_MIN_TRIANGLES)
                return true;
        if(triangles == null || enfants != null)
            return false;

        return false;
    }

    @Override
    public boolean isSolid() {
        return false;
    }
}
