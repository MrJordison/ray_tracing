package SimpleRaytracer.Geometry.accel_structs;

import SimpleRaytracer.Geometry.*;
import SimpleRaytracer.Geometry.csg.CSGThing;
import SimpleRaytracer.Material.Material;
import SimpleRaytracer.Math.Ray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Fonctionnement de la construction:
 *  - Appel du constructeur pour la racine où l'on va construire la boite à partir de la liste des triangles
 *  et les stocker temporairement. La construction récursive des enfants se fait selon les primitives (depth, nb min de triangles)
 *  - Cette construction récursive se déclenche uniquement si l'on veut faire intersecter un rayon avec l'octree.
 *  - Si il y a intersection avec la racine / un noeud parent (plus précisément avec sa boite englobante),
 *  on lance la génération des enfants (si ils n'existent pas), auquels on passe la boite, la liste des triangles du parent et leurs position (1 à 8).
 *  - A la fin de la génération, on supprime la liste des triangles du parent (évite la redondance d'informations dans l'arborescence de l'octree.
 *  - Le parent retourne l'intersection trouvée par les enfants
 *  - L'enfant construit sa sous boite d'après sa position et la boite du parent et va stocker uniquement les triangles qui
 *  intersectent avec sa sous boite.
 *  - Un enfant feuille (ne respecte plus les primitives depth, nb min de triangles) ne construit pas d'enfants, et va renvoyer (si existe)
 *  l'intersection avec un des triangles de sa liste
 */
public class Octree implements Thing, CSGThing {

    static final int OCTREE_MAX_DEPTH = 8;
    static final int OCTREE_MIN_TRIANGLES = 15;

    int position;
    IntersectBox boite;
    List<Triangle> triangles;
    Material material;
    Octree parent;
    List<Octree> enfants;

    public List<Triangle> test  = new ArrayList<>();


    //constructeur pour le parent
    public Octree(List<Triangle> triangles){
        this.parent = null;
        this.position = -1;
        this.triangles = triangles;
        enfants = null;
        boite = new IntersectBox(triangles);
        material = null;
    }

    //constructeur pour le parent
    public Octree(Mesh mesh){
        this.parent = null;
        this.position = -1;
        this.triangles = mesh.getFaces();
        enfants = null;
        boite = new IntersectBox(this.triangles);
        material = mesh.getMaterial();
    }

    //constructeur général => utilisé pour les enfants
    private Octree(List<Triangle> triangles, Octree parent, int position, IntersectBox boite){
        this.parent = parent;
        this.position = position;
        this.material = parent.getMaterial();

        //si c'est la racine, on calcule la boite englobante de base
        //et l'on garde la liste des triangles passée en paramètre
        if(position == -1) { // sous-entendu parent == null vu que c'est la racine
            boite = new IntersectBox(triangles);
            this.triangles = triangles;
        }
        //Sinon c'est un enfant, on calcule la sous boite d'après la position et la boite passée en paramètre
        //et l'on stocke uniquement les triangles passés en paramètre qui intersecte avec cette dernière
        else{
            this.boite = boite.getChild(position);
            this.triangles = new ArrayList<>();
            for(Triangle t : triangles)
                if(this.boite.intersectTriangle(t)) {
                    this.triangles.add(t);
                }
        }

    }

    public Intersection intersect(Ray ray){

        /*Intersection result, i;
        result = i = null;

        double[] tab = new double[5];

        //Test si il y a intersection entre la boite d'intersection et le rayon
        if(boite.intersectRay(ray,tab)){

            //Cas où les enfants n'existent pas
            if (enfants == null) {

                //Soit il n'y a jamais eu d'intersection et donc c'est un parent (=> donc il faut créer les enfants)
                if(!isLeaf()){
                    enfants = new ArrayList<>();
                    for(int j = 1 ; j <=8; ++j){
                        enfants.add(new Octree(this.triangles, this, j, this.boite));
                    }

                    //Elimination redondance des triangles
                    this.triangles = null;
                }

                //Soit c'est une feuille de l'octree(les tests des primitives renvoient vrai (méthode isLeaf()) (on fait les tests
                //d'intersection avec les triangles de la feuille)
                else{
                    for(Triangle t : triangles){
                        i = t.intersect(ray);
                        if(i != null) {
                            if (result == null)
                                result = i;
                            else if (i.m_RayDistance < result.m_RayDistance)
                                result = i;
                        }
                    }
                }
            }


            //Cas où les enfants existent déjà : il y a déjà eu test d'intersection avant
            if(enfants != null){
                //Pour chaque enfant
                for(Octree child : enfants){
                    //On récupère l'intersection renvoyée par ce dernier (si il y en a une)
                    i = child.intersect(ray);
                    //Si une intersection est trouvée, et que la distance avec le point d'origine
                    //du rayon est inférieure, alors on la conserve comme le résultat à retourner
                    if(i != null) {
                        if (result == null)
                            result = i;
                        else if (i.m_RayDistance < result.m_RayDistance)
                            result = i;
                    }
                }
            }
        }

        return result;*/

        Intersection result, temp_inters;
        result = temp_inters = null;
        double[] tab = new double[6];

        if(boite.intersectRay(ray, tab)){

            //Si c'est un noeud
            if(!isLeaf()){
                //System.out.println("Noeud !");
                for(Octree child : enfants){
                    temp_inters = child.intersect(ray);
                    if(temp_inters != null) {
                        if (result == null)
                            result = temp_inters;
                        else if(result != null && temp_inters.m_RayDistance < result.m_RayDistance)
                            result = temp_inters;
                    }
                }
            }

            //Si c'est une feuille
            else{
                //System.out.println("Feuille !");
                for(Triangle t : triangles){
                    temp_inters = t.intersect(ray);
                    if(temp_inters != null) {
                        if (result == null)
                            result = temp_inters;
                        else if(result != null && temp_inters.m_RayDistance < result.m_RayDistance)
                            result = temp_inters;
                    }
                }
            }


        }//Fin de condition sur l'intersection avec la boite englobante

        if(result != null) {
            boolean b = false;
            for (Triangle t : test)
                b |= (Object) t == (Object) result.m_Thing;
            if (!b)
                test.add((Triangle) result.m_Thing);
        }

        return result;
    }

    public List<Intersection> intersectCSG(Ray ray){

        //Création et initialisation des variables nécéssaires
        List<Intersection> result_intersections = new ArrayList<>(); // liste d'intersections à retourner

        double[] tab = new double[6]; //tableau nécéssaire pour fonction intersection de boite englobante

        //Si le rayon intersecte avec la boite englobante du noeud courant
        if(boite.intersectRay(ray, tab)){

            //Si c'est un noeud parent
            if(!isLeaf()){
                
                List<Intersection> temp_list; // liste d'intersections temporaire
                
                //pour chaque enfant
                for(Octree child : enfants){
                    
                    //Récupération des intersections des enfants si existent et l'ajoute à la liste courante
                    temp_list = child.intersectCSG(ray);
                    if(temp_list != null) {
                        result_intersections.addAll(temp_list);
                    }
                }
            }

            //Si c'est une feuille
            else{
                
                Intersection temp_inters = null; //variable temporaire pour stocker intersections des triangles
                
                //pour chaque triangle englobé dans la feuille
                for(Triangle t : triangles){
                    //calcul de l'intersection rayon - triangle courant
                    temp_inters = t.intersect(ray);
                    
                    //si il y a intersection, on l'ajoute à la liste à renvoyer
                    if(temp_inters != null) {
                        result_intersections.add(temp_inters);
                    }
                }
            }


        }//Fin de condition sur l'intersection avec la boite englobante
        
        //tri du tableau d'intersections d'après distance croissante avant de le retourner
        Collections.sort(result_intersections);

        return result_intersections;
    }
    

    public Material getMaterial(){
        return material;
    }
    public void setMaterial(Material material){this.material = material;
    for(Triangle t : triangles)
    t.setMaterial(material);}

    public boolean isSolid(){
        return false;
    }
    
    //Méthode pour construire boite englobante selon parent (ou boite actuelle si racine)
    public IntersectBox getBox(){
        //Si parent est null, alors la boite est celle de base de l'octree (créé dans le constructeur)
        if(parent == null)
            return boite;
        //Sinon c'est un enfant et ne possède pas forcément de boite(crée dynamiquement lors des tests d'intersections passant par cette boite)
        else{
            //Si la sous-boite est déjà construite, on la retourne directement
            if(boite != null)
                return boite;

            //Sinon la sous-boite se construit selon sa position(choix définis en dur dans la conception)
            //et est géré au sein de la classe IntersectBox
            else
                return parent.getBox().getChild(position);
        }
    }

    public int getDepth(){
        int res = 1;
        if(parent != null)
            res += parent.getDepth();
        return res;
    }

    public boolean isLeaf(){
        if(getDepth() >= OCTREE_MAX_DEPTH)
            return true;
        if(triangles != null)
            if(triangles.size() <= OCTREE_MIN_TRIANGLES)
                return true;

        if(triangles == null || position == -1 || enfants != null)
            return false;
        return false;
    }


    public void runGeneration(){
        //Tant que c'est un parent, lance la génération d'enfants
        if(!isLeaf()) {
            System.out.println("Parent n° "+position+" à la profondeur "+getDepth()+" lance la génération de gniards");
            if (enfants == null) {
                enfants = new ArrayList<>();
                for (int i = 1; i <= 8; ++i) {
                    enfants.add(new Octree(this.triangles, this, i, this.boite));
                }
                triangles = null;
            }
            for(Octree enfant : enfants)
                enfant.runGeneration();
        }
        else
            System.out.println("Enfant n°"+position+" a atteint la génération maximale profondeur "+getDepth()+" nb triangles :"+triangles.size());
    }

    public void reassembleTriangle(List<Triangle> triangles){

        if(enfants == null){
            boolean result = false;
            for(Triangle t : this.triangles){
                result = false;
                for(Triangle t2 : triangles)
                    result |= t==t2;
                if(!result)
                    triangles.add(t);
            }

        }

        else {
            for (Octree e : enfants)
                e.reassembleTriangle(triangles);
        }
    }


}
