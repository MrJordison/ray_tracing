package SimpleRaytracer.Geometry.csg;

import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Geometry.Thing;
import SimpleRaytracer.Material.Material;
import SimpleRaytracer.Math.Ray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CSG implements Thing, CSGThing {

    public static final int CSG_DIFFERENCE = 0;
    public static final int CSG_INTERSECTION = 1;
    public static final int CSG_UNION = 2;

    int boolean_operation;
    CSGThing left_child;
    CSGThing right_child;

    public CSG(CSGThing thing_1, CSGThing thing_2, int boolean_operation){
        this.left_child = thing_1;
        this.right_child = thing_2;
        setBooleanOperation(boolean_operation);
    }

    public CSGThing getLeftChild(){return this.left_child;}
    public CSGThing getRightChild(){return this.right_child;}

    public void setLeftChild(CSGThing thing){this.left_child = thing;}
    public void setRightChild(CSGThing thing){this.right_child = thing;}
    public boolean setBooleanOperation(int boolean_operation) {
        if (boolean_operation != CSG_DIFFERENCE && boolean_operation != CSG_INTERSECTION && boolean_operation != CSG_UNION){
            System.err.println("Type d'opération booléenne spécifiée incorrecte");
            return false;
        }

        this.boolean_operation = boolean_operation;
        return true;
    }

    @Override
    public Intersection intersect(Ray ray){
        List<Intersection> resultCSG = intersectCSG(ray);
        if(resultCSG.size() == 0)
            return null;
        else
            return resultCSG.get(0);
    }

    public List<Intersection> intersectCSG(Ray ray){
        List<Intersection> result = new ArrayList<>();

        List<Intersection> inters_left = left_child.intersectCSG(ray);
        List<Intersection> inters_right = right_child.intersectCSG(ray);

        double min1,max1, min2, max2;

        switch(boolean_operation){

            // Toutes les intersections de left_child et right_child
            case CSG_UNION:
                result.addAll(inters_left);
                result.addAll(inters_right);
                break;

            //Les intersections appartenant à left et right uniquement
            // <= max1 && >= min2 || <= max2 && >= min1
            case CSG_INTERSECTION:
                //Si un des objets ne renvoient pas d'intersection
                // il n'y a rien à afficher
                if(inters_left.size() == 0 || inters_right.size() == 0)
                    break;

                //Sinon ...

                min1 = inters_left.get(0).m_RayDistance;
                max1 = inters_left.get(inters_left.size()-1).m_RayDistance;
                min2 = inters_right.get(0).m_RayDistance;
                max2 = inters_right.get(inters_right.size()-1).m_RayDistance;

                //valide les intersections de left si elles sont comprises dans l'intervalle de right
                for(Intersection i : inters_left)
                    if(i.m_RayDistance >= min2 && i.m_RayDistance <= max2)
                        result.add(i);

                //valide les intersections de right 2 si elles sont comprises dans l'intervalle left
                for(Intersection i : inters_right)
                    if(i.m_RayDistance >= min1 && i.m_RayDistance <= max1)
                        result.add(i);

                break;

            //Les intersections appartenant à left mais pas à right
            case CSG_DIFFERENCE:
                //Si left n'a pas d'intersections
                //il n'y a rien à afficher
                if(inters_left.size() == 0)
                    break;

                //Si right n'a pas d'intersection
                //On affiche simplement left
                if(inters_right.size() == 0){
                    result = inters_left;
                    break;
                }

                //Sinon ...

                min1 = inters_left.get(0).m_RayDistance;
                max1 = inters_left.get(inters_left.size()-1).m_RayDistance;
                min2 = inters_right.get(0).m_RayDistance;
                max2 = inters_right.get(inters_right.size()-1).m_RayDistance;

                //valide les intersections de left qui n'appartiennent pas à right
                for(Intersection i : inters_left)
                    if(i.m_RayDistance < min2 || i.m_RayDistance > max2)
                        result.add(i);

                //valide les intersections de right qui appartiennent à left mais
                //on inverse la normale associée
                for(Intersection i : inters_right)
                    if(i.m_RayDistance >= min1 && i.m_RayDistance <= max1) {
                        i.m_Normal = i.m_Normal.mul(-1);
                        result.add(i);
                    }

                break;
        }

        //Tri des intersections validées par distance croissante
        Collections.sort(result);

        return result;
    }

    @Override
    public Material getMaterial() {
        return null;
    }

    @Override
    public boolean isSolid() {
        return false;
    }
}
