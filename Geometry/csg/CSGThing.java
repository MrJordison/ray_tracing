package SimpleRaytracer.Geometry.csg;

import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Math.Ray;

import java.util.List;

/**
 * Created by mstr_jordison on 24/03/17.
 */
public interface CSGThing {
    public List<Intersection> intersectCSG(Ray ray);
}
