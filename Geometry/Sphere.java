package SimpleRaytracer.Geometry;

import SimpleRaytracer.Geometry.csg.CSGThing;
import SimpleRaytracer.Material.Material;
import SimpleRaytracer.Math.Ray;
import SimpleRaytracer.Math.Vector;

import java.util.ArrayList;
import java.util.List;

public class Sphere implements Thing, CSGThing{

	private Vector center;
	private double radius;

	private Material material;

	public Sphere(Vector center, double radius){
	    this.center = center;
	    this.radius = radius;
    }

    public Sphere(Vector center, double radius, Material material){
        this.center = center;
        this.radius = radius;
        setMaterial(material);
    }

	@Override
	public Intersection intersect(Ray ray) {

        List<Intersection> result = intersectCSG(ray);
        if(result.size() == 0)
            return null;
        else
            return result.get(0);
	}

	public List<Intersection> intersectCSG(Ray ray){
	    List<Intersection> result = new ArrayList<>();

        //Geometric method
        Intersection inters;
        double distIntersect;
        Vector intersectPoint;
        Vector normaleIntersectPoint;

        //Création du vecteur entre l'origine du rayon et le centre de la sphère (orienté origin -> centre)
        Vector originRaySphereCenter = center.sub(ray.m_Origin);

        //Calcul de la longueur du projeté de originRaySphereCenter sur le vecteur de direction du rayon
        double tangent_center_sphere = ray.m_Direction.dot(originRaySphereCenter);

        //Si cette longueur est strict. négative, il n'y a pas d'intersection (intersection derrière caméra donc non visible)
        if(tangent_center_sphere < 0.)
            return result;

        //Si cette longueur est strict. positive, alors il y a intersection avec la sphère
        double orthoDistCenterRay = Math.sqrt(originRaySphereCenter.dot(originRaySphereCenter) - tangent_center_sphere * tangent_center_sphere);

        if(orthoDistCenterRay < 0 || orthoDistCenterRay > radius)
            return result;


        double distFromOrthoVecCenterRay = Math.sqrt(radius * radius - orthoDistCenterRay * orthoDistCenterRay);

        distIntersect = tangent_center_sphere - distFromOrthoVecCenterRay;
        intersectPoint = ray.m_Origin.add(ray.m_Direction.mul(distIntersect));
        normaleIntersectPoint = intersectPoint.sub(center).norm();
        inters = new Intersection(this,ray,  distIntersect, normaleIntersectPoint);
        result.add(inters);

        distIntersect = tangent_center_sphere + distFromOrthoVecCenterRay;
        intersectPoint = ray.m_Origin.add(ray.m_Direction.mul(distIntersect));
        normaleIntersectPoint = intersectPoint.sub(center).norm();
        inters = new Intersection(this,ray,  distIntersect, normaleIntersectPoint);
        result.add(inters);

	    return result;
    }

	@Override
	public Material getMaterial() {
		return material;
	}

	public boolean setMaterial(Material material){
        if(material == null) {
            System.err.println("NullPointerError : "+(Object)this.toString() + "\nTentative de changer le Material à null");
            return false;
        }
        this.material = material;
        return true;
    }

	@Override
	public boolean isSolid() {
		// TODO Auto-generated method stub
		return false;
	}

}
