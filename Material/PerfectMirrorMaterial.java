package SimpleRaytracer.Material;

import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Material.utils.MaterialUtils;
import SimpleRaytracer.Math.Ray;
import SimpleRaytracer.Math.Vector;
import SimpleRaytracer.Rendering.Camera;
import SimpleRaytracer.Rendering.Color;
import SimpleRaytracer.Rendering.Light;
import SimpleRaytracer.Rendering.Raytracer;

public class PerfectMirrorMaterial extends Material {
    @Override
    public Color shade(Intersection isect, Light light, Raytracer tracer, Camera camera, double a_RIndex, int depth) {

        Color result;

        Vector normale = isect.m_Normal.norm();

        Vector position = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));

        Vector vue = camera.m_Position.sub(position).norm();

        Vector light_direction = light.m_Position.sub(position).norm();

        Vector reflect_direction = MaterialUtils.reflect(vue, normale).norm();

        Vector rayStartPosition = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance - 0.0001));

        result = tracer.traceSecondaryRay(new Ray(rayStartPosition,reflect_direction),depth + 1, light, a_RIndex);

        return result;

    }
}
