package SimpleRaytracer.Material;


import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Material.utils.MaterialUtils;
import SimpleRaytracer.Math.Vector;
import SimpleRaytracer.Rendering.Camera;
import SimpleRaytracer.Rendering.Color;
import SimpleRaytracer.Rendering.Light;
import SimpleRaytracer.Rendering.Raytracer;

public class PhongMaterial extends Material{

	private Vector coeffs_light;
	private Color specular_color, diffus_color;
	private int coeffMaterial;

	
	public PhongMaterial(Color color, Vector coeffs_light, int coeffMaterial){
		this.coeffs_light = coeffs_light;
		this.coeffMaterial = coeffMaterial;
		this.diffus_color = color;
		this.specular_color = new Color(1.);
	}
	public PhongMaterial(Color diffus_color, Color specular_color, Vector coeffs_light, int coeffMaterial){
		this.coeffs_light = coeffs_light;
		this.coeffMaterial = coeffMaterial;
		this.diffus_color = diffus_color;
		this.specular_color = specular_color;
	}
	
	@Override
	public Color shade(Intersection isect, Light light, Raytracer tracer, Camera camera, double a_RIndex, int depth) {

		Vector normale = isect.m_Normal.norm();
		
		Vector position = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));

		Vector vue = position.sub(camera.m_Position).norm();

		Vector light_direction = light.m_Position.sub(position).norm();

		Vector reflect_direction = MaterialUtils.reflect(light_direction,normale).norm();

		double light_quantity = MaterialUtils.LightQuantity(light_direction, normale);
		
        Color diffus_composante = diffus_color.scale(light_quantity).scale(coeffs_light.m_X);

        double specular_value = Math.pow(Math.max(0, reflect_direction.dot(vue)),coeffMaterial);
        //specular_value = specular_value / light_quantity;

        boolean test = Double.isNaN(specular_value) || (specular_value == Double.POSITIVE_INFINITY);

        if(test)
            specular_value = 0.;

        Color specular_composante = specular_color.scale(specular_value).scale(coeffs_light.m_Y);

        return diffus_composante.add(specular_composante);
	}


	

}
