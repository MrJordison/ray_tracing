package SimpleRaytracer.Material;

import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Rendering.Camera;
import SimpleRaytracer.Rendering.Color;
import SimpleRaytracer.Rendering.Light;
import SimpleRaytracer.Rendering.Raytracer;

public class FlatColorMaterial extends Material{
	
	private Color color;
	
	public FlatColorMaterial(double r, double g, double b){
		this.color = new Color(r,g,b);
	}
	
	public FlatColorMaterial(Color color){
		this.color = color;
	}

	@Override
	public Color shade(Intersection isect, Light light, Raytracer tracer, Camera camera, double a_RIndex, int depth) {
		return color;
	}
	
}
