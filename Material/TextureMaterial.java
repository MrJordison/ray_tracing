package SimpleRaytracer.Material;

import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Geometry.Triangle;
import SimpleRaytracer.Material.utils.MaterialUtils;
import SimpleRaytracer.Math.Vector;
import SimpleRaytracer.Rendering.*;
import SimpleRaytracer.io.TextureIO;

public class TextureMaterial extends Material {

    Texture tex;

    public TextureMaterial(String filename){
        this.tex = TextureIO.in(filename);
    }

    public Texture getTexture(){return tex;}
    public void setTexture(Texture texture){tex = texture;}
    public void setTexture(String filename){tex = TextureIO.in(filename);}



    @Override
    public Color shade(Intersection isect, Light light, Raytracer tracer, Camera camera, double a_RIndex, int depth) {
        Vector position = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));
        Triangle t = (Triangle)isect.m_Thing;

        Color ambient = MaterialUtils.sample_texture(tex, t, position);

        return ambient;
    }


}
