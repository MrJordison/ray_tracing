package SimpleRaytracer.Material;

import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Material.utils.MaterialUtils;
import SimpleRaytracer.Math.Ray;
import SimpleRaytracer.Math.Vector;
import SimpleRaytracer.Rendering.*;

public class GlassMaterial extends Material{

    double ior = 1.524;

    @Override
    public Color shade(Intersection isect, Light light, Raytracer tracer, Camera camera, double a_RIndex, int depth) {


        Color result;

        Vector normale = isect.m_Normal.norm();

        Vector position = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));

        Vector vue = camera.m_Position.sub(position).norm();

        Vector light_direction = light.m_Position.sub(position).norm();

        double ior_in, ior_out;

        ior_in = a_RIndex ;
        ior_out = a_RIndex != this.ior ? this.ior : Constants.GLOBAL_IOR;

        Vector refractVector = MaterialUtils.refract(isect.m_Ray.m_Direction, normale, ior_in, ior_out);

        //Si refractVector == null, alors il n'y a que de la réflexion
        if(refractVector == null) {
            return new Color(0.);
        }
        //double reflect_coeff = MaterialUtils.schlickApproximation(vue, normale, ior_in, ior_out);


        refractVector = refractVector.norm();

        Vector ecart = normale.mul(0.001);

        Vector rayStartPosition = position;

        //Si le rayon incident est à l'extérieur de la surface, on soustrait l'écart à la position du point d'intersection
        //pour être sûr que le raytrace du réfracté commence à l'intérieur
        if(isect.m_Ray.m_Direction.dot(normale) < 0)
            rayStartPosition = rayStartPosition.sub(ecart);
        //Sinon le rayon est à l'intérieur, il faut ajouter l'écart pour faire commencer le raytrace du refracté à l'intérieur
        else
            rayStartPosition = rayStartPosition.add(ecart);

        result = tracer.traceSecondaryRay(new Ray(rayStartPosition,refractVector),depth + 1, light, ior_out);

        return result;
    }
}
