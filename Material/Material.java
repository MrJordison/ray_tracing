package SimpleRaytracer.Material;

import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Rendering.Camera;
import SimpleRaytracer.Rendering.Color;
import SimpleRaytracer.Rendering.Light;
import SimpleRaytracer.Rendering.Raytracer;

public abstract class Material
{
	public abstract Color shade(Intersection isect, Light light, Raytracer tracer, Camera camera, double a_RIndex, int depth);
}
