package SimpleRaytracer.Material.utils;

import SimpleRaytracer.Geometry.Triangle;
import SimpleRaytracer.Math.Vector;
import SimpleRaytracer.Rendering.Color;
import SimpleRaytracer.Rendering.Texture;

public class MaterialUtils {

    public static Vector reflect(Vector incidence_vector, Vector normale){
        Vector res = normale.mul(2. * incidence_vector.dot(normale)).sub(incidence_vector);
        return res;
    }

    public static double LightQuantity(Vector light, Vector normale){
        return Math.max(0, normale.dot(light));
    }


    public static Color sample_texture(Texture tex, Triangle t, Vector position){
        Vector cb = t.getBarycentricsCoeffs(position);

        Texture texture = tex == null ? t.getTexture() : tex;

        //Calcul des coordonnées de textures uv avec interpollation barycentrique
        Vector uv1 = t.getUVCoordVertice(t.getVertices().get(0)).mul(cb.m_X);
        Vector uv2 = t.getUVCoordVertice(t.getVertices().get(1)).mul(cb.m_Y);
        Vector uv3 = t.getUVCoordVertice(t.getVertices().get(2)).mul(cb.m_Z);

        Vector uv = uv1.add(uv2).add(uv3);

        //Mapping avec correction perspective
        Color result =  texture.getPixelUV(uv);


        return result;
    }

    public static Vector refract(Vector incident_vector, Vector normale, double ior_incident, double ior_refract){
        Vector result;

        double cos1 = normale.dot(incident_vector);

        Vector N = normale;

        if(cos1 < 0)
            cos1 = -cos1;
        else
            N = N.mul(-1);

        double divIors = ior_incident / ior_refract;

        double bla = divIors * divIors;
        double bla2 = 1 - cos1 * cos1;

        double cos2 = 1. - bla * bla2;

        if(cos2 < 0.)
            result = null;
        else
            result = incident_vector.mul(divIors).add(
                    normale.mul(divIors * cos1 - Math.sqrt(cos2))
            );

        return result;
    }

    public static double schlickApproximation(Vector incident_vector, Vector normale, double ior_incident, double ior_refract){
        double result;

        double cos = normale.dot(incident_vector);

        double r0 = Math.pow((ior_incident - ior_refract) / (ior_incident + ior_refract), 2.);

        result = r0 + (1 - r0) * Math.pow(1 - cos,5.);
        return result;
    }

}
