package SimpleRaytracer;

import SimpleRaytracer.Geometry.IntersectBox;
import SimpleRaytracer.Geometry.Octree;
import SimpleRaytracer.Geometry.Triangle;
import SimpleRaytracer.Math.Vector;
import SimpleRaytracer.io.MeshIO;

import java.util.ArrayList;
import java.util.List;

public class MainTest {
    public static void main(String[] args){
        List<Triangle> triangles = MeshIO.ObjPtsIn("/media/mstr_jordison/IOUSBY/Cours/M1/Semestre 2/ISIR/SimpleRaytracer/resources/Lightning/lightning_obj.obj");
        Octree o  = new Octree(triangles);
        o.runGeneration();
        List<Triangle> test = new ArrayList<>();
        o.reassembleTriangle(test);
        System.out.println("nb pointeurs de triangles : "+test.size() + "nb triangles départ : "+triangles.size());

    }
}
