package SimpleRaytracer.Rendering;

public class Constants {
    public static final int MAXSECONDAIRES = 5;
    public static final double GLOBAL_IOR = 1.000272;
}
