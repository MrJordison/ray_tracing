package SimpleRaytracer.Rendering;

import java.awt.image.BufferedImage;

import SimpleRaytracer.Geometry.Intersection;
import SimpleRaytracer.Geometry.Thing;
import SimpleRaytracer.Material.Material;
import SimpleRaytracer.Math.Ray;
import SimpleRaytracer.Math.Vector;

public class Raytracer
{
	private double m_LastDist;

	Scene scene;
	
	private Intersection intersect(Ray ray, Scene scene)
	{
		double closest = Double.POSITIVE_INFINITY;
		Intersection closestIntersection = null;
		
		for (Thing thing : scene.m_Things)
		{
			Intersection inter = thing.intersect(ray);
			if (inter != null && inter.m_RayDistance < closest)
			{
				closestIntersection = inter;
				closest = inter.m_RayDistance;
			}
		}
		
		return closestIntersection;
	}
	
	public Color traceRay(Ray ray, Scene scene, int depth, double a_RIndex)
	{
		Intersection isect = intersect(ray, scene);
		if (isect == null)
		{
			return Color.Background;
		}
		else
		{
			m_LastDist = isect.m_RayDistance;
			Color c = shade(isect, scene, depth, a_RIndex);

            return c;
		}
	}

	public Color traceSecondaryRay(Ray ray, int depth, Light light, double a_RIndex)
	{
	    if(depth > Constants.MAXSECONDAIRES)
	        return Color.Background;

		Intersection isect = intersect(ray, this.scene);
		if (isect == null)
		{
			return Color.Background;
		}
		else
		{
			m_LastDist = isect.m_RayDistance;

            Material material = isect.m_Thing.getMaterial();

            Color color = material.shade(isect, light, this, scene.m_Camera, a_RIndex, depth);

			return color;
		}
	}

	private Color shade(Intersection isect, Scene scene, int depth, double a_RIndex)
	{
		Material material = isect.m_Thing.getMaterial();

		Color c = new Color(0,0,0);
		for (Light light : scene.m_Lights)
		{
			Color color = material.shade(isect, light, this, scene.m_Camera, a_RIndex, 0);
			c = c.add(color);
        }

		return c;
	}
	
	// Exercice A - expliquer brievement
	private Vector getPoint(int x, int y, Camera camera, int width, int height)
	{
		// Example x going from 0 to 640, excluded
		// Then dx is between -320.0 and 320.0 (excluded)
		double dx = (double)x - (width / 2.0);
		// Then dx is between -0.5 and 0.5 (excluded)
		dx /= (double)Math.max(width, height);
		double dy = (double)y - (height / 2.0);
		dy /= (double)Math.max(width, height);

        Vector result = camera.m_RightDirection.mul(dx).add(camera.m_UpDirection.mul(dy)).add(camera.m_ForwardDirection.mul(camera.m_FocalDistance)).norm();
        return result;
	}
	
	// Exercice A - expliquer brievement
	public void render(Scene scene, BufferedImage image)
	{
	    //Garde la scene en attribut durant le rendu pour le lancer de rayons secondaires (éviter de la passer en paramètre de la fonction shade(...))
		this.scene = scene;

		int width = image.getWidth();
		int height = image.getHeight();
		
		for (int y=0; y < height; y++)
		{
			System.out.println("Raytracing scanline: " + (y+1) + "/" + height);
			for (int x=0; x < width; x++)
			{
				Vector dir = getPoint(x, y, scene.m_Camera, width, height);
				Color color = traceRay(new Ray(scene.m_Camera.m_Position, dir), scene, 0, /* a_RIndex initial */ Constants.GLOBAL_IOR);
				int rgb = color.toRGB();
				image.setRGB(x, height-y-1, rgb);
			}
		}

		this.scene = null;

		System.out.println("Complete.");
	}
}
