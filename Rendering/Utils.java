package SimpleRaytracer.Rendering;

public class Utils {
    public static double clamp(double value, double min, double max){
        if(value > max)
            return 1.;
        if(value < min)
            return 0.;
        return value;
    }

    public static boolean testFileExtension(String filename, String extension){
        int index = filename.lastIndexOf(".");
        String filetype = filename.substring(index+1);
        return filetype.equals(extension);
    }
}
