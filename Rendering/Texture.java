package SimpleRaytracer.Rendering;

import SimpleRaytracer.Math.mat.Matrix;
import SimpleRaytracer.Math.Vector;

public class Texture{
    private Matrix<Color> pixels_grid;

    public Texture(){
        pixels_grid = new Matrix<>();
    }

    public Texture(Matrix<Color> grid){
        pixels_grid = grid;
    }

    public Texture(int[] dimensions, Color default_color){
        pixels_grid = new Matrix<Color>(dimensions, default_color);
    }

    public Texture(int[] dimensions){
        pixels_grid = new Matrix<Color>(dimensions, new Color());
    }

    public int[] getDimensions(){return pixels_grid.getDimensions();}

    public Color getPixel(int row, int col){return pixels_grid.get_value(row, col);}

    public Color getPixelUV(Vector uv){

        int i = (int)(uv.m_Y * (double)(getDimensions()[0]-1));
        int j = (int)(uv.m_X * (double)(getDimensions()[1]-1));
        //System.out.println(uv+" : "+i+";"+j);
        return getPixel(Math.floorMod(i,getDimensions()[0]),Math.floorMod(j,getDimensions()[1]));
    }

    public void setPixel(int row, int col, Color c){pixels_grid.set_value(row, col, c);}

    public Texture clone(){return new Texture(pixels_grid.clone());}
}
