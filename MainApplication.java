package SimpleRaytracer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import SimpleRaytracer.Geometry.Mesh;
import SimpleRaytracer.Geometry.Sphere;
import SimpleRaytracer.Geometry.accel_structs.Octree;
import SimpleRaytracer.Geometry.Thing;
import SimpleRaytracer.Geometry.Triangle;
import SimpleRaytracer.Geometry.csg.CSG;
import SimpleRaytracer.Material.*;
import SimpleRaytracer.Math.Vector;
import SimpleRaytracer.Rendering.*;
import SimpleRaytracer.io.MeshIO;
import SimpleRaytracer.io.TextureIO;

public class MainApplication
{
	public static void main(String[] args)
	{
		BufferedImage bi = new BufferedImage(800, 800, BufferedImage.TYPE_INT_RGB);


		List<Triangle> triangles;

		/*
		List<Vector> pts = new ArrayList<>();
        pts.add(new Vector(-0.5,-0.5,-5));
        pts.add(new Vector(0.5,-0.5,-5));
        pts.add(new Vector(-0.5,0.5,-6));

        List<Vector> normales = new ArrayList<>();
        normales.add(new Vector(0.,1.,1));
        normales.add(new Vector(-1,1.,1));
        normales.add(new Vector(0.5,1.,2));

		List<Vector> uvs = new ArrayList<>();
		uvs.add(new Vector(0.));
		uvs.add(new Vector(1.,0.,0.));
		uvs.add(new Vector(0.,1.,0.));

        Triangle t = new Triangle(pts, normales, uvs);


        List<Vector> pts2 = new ArrayList<>();
        pts2.add(new Vector(0.5,-0.5,-5));
        pts2.add(new Vector(-0.5,0.5,-6));
        pts2.add(new Vector(0.5,0.5,-6));

        List<Vector> normales2 = new ArrayList<>();
        normales2.add(new Vector(0.,1.,1));
        normales2.add(new Vector(1,1.,1));
        normales2.add(new Vector(0.5,1.,2));

        List<Vector> uvs2 = new ArrayList<>();
        uvs2.add(new Vector(1.,0.,0.));
        uvs2.add(new Vector(0.,1.,0.));
        uvs2.add(new Vector(1,1.,0.));


        Triangle t2 = new Triangle(pts2, normales2, uvs2);

		Texture tex = TextureIO.in("/media/mstr_jordison/IOUSBY/Cours/M1/Semestre 2/ISIR/SimpleRaytracer/resources/damier.png");
        t.setTexture(tex);
        t2.setTexture(tex);

        Material material = new TextureMaterial();

        triangles = new ArrayList<>();
        triangles.add(t);
        triangles.add(t2);*/

		/* OBJ différents types de Phong + LafortuneMaterial */
        triangles = MeshIO.ObjPtsIn("/media/mstr_jordison/IOUSBY/Cours/M1/S2/ISIR/SimpleRaytracer/resources/lara.obj",
                new Vector(-0.02,-1.,-5));
        //Material material = new PhongMaterial(new Color(192./ 255.), new Vector(0.6,0.4,0.),5);
        Material material = new LaFortuneMaterial(new Color(192./ 255.), new Vector(0.5,0.5,0.), new Vector(2.,0.25,1),3);

        Octree o = new Octree(triangles);
        o.setMaterial(material);
        o.runGeneration();

        List<Triangle> tr2  = MeshIO.ObjPtsIn("/media/mstr_jordison/IOUSBY/Cours/M1/S2/ISIR/SimpleRaytracer/resources/plane.obj",
                new Vector(0.,-1.1,-5));
        Material m2 = new PhongMaterial(new Color(184./ 255., 115. / 255., 51. / 255.), new Vector(0.6,0.4,0.),5);
        Octree plane = new Octree(tr2);
        plane.runGeneration();
        plane.setMaterial(m2);


        /* Miroir parfait */
        /*
        Sphere s = new Sphere(new Vector(-1,0,-5), 0.5);
        Material m3 = new PerfectMirrorMaterial();
        s.setMaterial(m3);

        */

        /* Verre */
        //Sphere s = new Sphere(new Vector(0,0,-4.45), 0.5);
        Octree cube = new Octree(MeshIO.ObjPtsIn("/media/mstr_jordison/IOUSBY/Cours/M1/S2/ISIR/SimpleRaytracer/resources/cube.obj", new Vector(0,0,-4.3)));
        Material m3 = new GlassMaterial();
        cube.setMaterial(m3);


        /* Affichage avec OBJ + texture */
        /*
        triangles = MeshIO.ObjPtsIn("/media/mstr_jordison/IOUSBY/Cours/M1/S2/ISIR/SimpleRaytracer/resources/turtle/turtle.obj",
                new Vector(-0.02,-1.,-5));
        //Material material = new PhongMaterial(new Color(0.,1,1), new Vector(0.6,0.4,0.),40);
        Material material = new TextureMaterial("/media/mstr_jordison/IOUSBY/Cours/M1/S2/ISIR/SimpleRaytracer/resources/turtle/turtle.png");

        Octree o = new Octree(triangles);
        o.setMaterial(material);
        o.runGeneration();
        */

        /* CSG Part */
        /*
		Sphere s = new Sphere(new Vector(-1,0,-5), 1., new PhongMaterial(new Color(1.,0,0), new Vector(0.6,0.2,0), 60));
        Sphere s2 = new Sphere(new Vector(0,0,-5), 1., new PhongMaterial(new Color(0,0,1.), new Vector(0.6,0.2,0), 60));
        CSG csg = new CSG(s, s2, CSG.CSG_DIFFERENCE);
        Sphere s3 = new Sphere(new Vector(-0.5,-0.5,-5), 1., new PhongMaterial(new Color(0,1.,0), new Vector(0.6,0.2,0), 60));
        CSG csg2 = new CSG(csg, s3, CSG.CSG_INTERSECTION);
        */

        Thing[] things =
			{
				o,
                plane,
                cube
			};
		
		Light[] lights =
			{
				//new Light(new Vector(-2.0, 2.5, 0.0), new Color(0.49, 0.07, 0.07)),
				//new Light(new Vector( 1.5, 2.5, 1.5), new Color(0.07, 0.07, 0.49)),
				//new Light(new Vector( 1.5, 2.5,-1.5), new Color(0.07, 0.49, 0.071)),
				new Light(new Vector( 0.0, 0, 5.0), new Color(0.21, 0.21, 0.35)),
                new Light(new Vector( 0.0, 0, -10.0), new Color(0.21, 0.21, 0.35))

            };
		
		Camera camera = new Camera(new Vector(0., 0., 0), new Vector(0, 0, -1.));
		
		Scene scene = new Scene(things, lights, camera);
		
		try
		{
			Raytracer raytracer = new Raytracer();
			long begin = System.currentTimeMillis();
			raytracer.render(scene, bi);
			long end = System.currentTimeMillis();
            System.out.println("Temps  : "+(end-begin)+" ms");
        }
		catch (Exception e)
		{
			e.printStackTrace();
		}
	
	    File outputfile = new File("result.png");
	    
	    try
	    {
	    	ImageIO.write(bi, "png", outputfile);
	    }
	    catch (IOException e)
	    {
	    	e.printStackTrace();
	    }
	}
}
