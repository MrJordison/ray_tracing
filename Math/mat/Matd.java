package SimpleRaytracer.Math.mat;

public class Matd extends Matrix<Double>{

    public Matd(){
        super();
    }

    public Matd(int[] dimensions){
        super(dimensions,0.);
    }


    public Matd(int[] dimensions, double default_value){
        super(dimensions,default_value);
    }

    public Matd(Double[][] values){
        super(values);
    }

    private Matd(Matrix<Double> m){
        this.dimensions = m.dimensions;
        this.values = m.values;
    }

    public Matd add(Matd m){
        if(m.dimensions[0] != dimensions[0] || m.dimensions[1] != dimensions[1])
            return null;

        Matd result = clone();
        for(int i = 0 ; i < values.size(); ++i)
            result.values.set(i, result.values.get(i) + m.values.get(i));

        return result;
    }

    public Matd sub(Matd m){
        if(m.dimensions[0] != dimensions[0] || m.dimensions[1] != dimensions[1])
            return null;

        Matd result = clone();
        for(int i = 0 ; i < values.size(); ++i)
            result.values.set(i, result.values.get(i) - m.values.get(i));

        return result;
    }

    public Matd mult(Matd m){
        if(dimensions[1] != m.dimensions[0])
            return null;

        Matd result = new Matd(new int[]{dimensions[0], m.dimensions[1]}, 0.);

        double temp;
        for(int i = 0; i < dimensions[0]; ++i)
            for(int j = 0 ; j < m.dimensions[1]; ++j)
                for(int k = 0 ; k < dimensions[1]; ++k) {
                    temp = get_value(i, k) * m.get_value(j, k);

                    result.set_value(i, j,
                            result.get_value(i, j) + ((Double.isNaN(temp) || Double.isInfinite(temp)) ? 0. : temp));
                }
        return result;
    }

    public Matd scale(double scalar){
        Matd result = clone();
        double temp;
        for(int i = 0 ; i < values.size(); ++i) {
            temp = result.values.get(i) * scalar;
            result.values.set(i, (Double.isNaN(temp) || Double.isInfinite(temp)) ? 0. : temp);
        }
        return result;
    }

    public Matd add_scalar(double scalar){
        Matd result = clone();
        for(int i = 0 ; i < values.size(); ++i)
            result.values.set(i, result.values.get(i) + scalar);

        return result;
    }

    public static Matd identity(int[] dimensions){
        Matd result = new Matd(dimensions, 0.);
        for(int i = 0 ; i < dimensions[0]; ++i)
            result.values.set(i*dimensions[1], 1.);

        return result;
    }

    public Matd clone(){
        return new Matd(super.clone());
    }

}
